package com.zara.store.prices.mapper;

import com.zara.store.prices.dto.PriceDto;
import com.zara.store.prices.model.PricesEntity;
import com.zara.store.prices.response.FindPriceResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PriceMapper {

   public PriceDto entityToDto(PricesEntity entity){
        PriceDto dto = new PriceDto();
        dto.setPriceList(entity.getPriceList());
        dto.setBrandId(entity.getBrandId());
        dto.setStartDate(entity.getStartDate().toString());
        dto.setEndDate(entity.getEndDate().toString());
        dto.setProductId(entity.getProductId());
        dto.setPriority(entity.getPriority());
        dto.setPrice(entity.getPrice());
        dto.setCurrency(entity.getCurrency());
        dto.setLastUpdate(entity.getLastUpdate());
        dto.setLastUpdateBy(entity.getLastUpdateBy());
        return dto;
    }

    public List<PriceDto> entityToDtoList (List<PricesEntity> entityList){
        List<PriceDto> priceDtoList = new ArrayList<>();
       for (PricesEntity e : entityList ){
           priceDtoList.add(this.entityToDto(e));
       }
       return priceDtoList;
    }

    public FindPriceResponse buildFindPriceResponse(PriceDto dto){
       return FindPriceResponse.builder()
               .priceList(dto.getPriceList())
               .productId(dto.getProductId())
               .brandId(dto.getBrandId())
               .startDate(dto.getStartDate())
               .endDate(dto.getEndDate())
               .price(dto.getPrice())
               .build();
    }
}
