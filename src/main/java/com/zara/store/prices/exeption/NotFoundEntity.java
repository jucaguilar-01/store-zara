package com.zara.store.prices.exeption;

public class NotFoundEntity extends Exception {

    private static final long serialVersionUID = 4718828512143293558L;

    public static String NOT_FOUND_ENTITY = "ERROR NOT FOUNT ENTITY IN DATA BASE";
    public static String MULTIPLES_ENTITIES = "ERROR THERE ARE MULTIPLES ENTITIES ";

    public NotFoundEntity(String error) {
        super(error);
    }
}
