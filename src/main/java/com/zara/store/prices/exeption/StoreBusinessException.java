package com.zara.store.prices.exeption;

public class StoreBusinessException extends Exception {

    private static final long serialVersionUID = 7718828512143293558L;

    public static String SERVICE_GET_PRICES = "ERROR IN SERVICE GET PRICES";

    private final String exceptionMessage;

    public StoreBusinessException(String exceptionMessage, String error) {
        super(error);
        this.exceptionMessage = exceptionMessage;
    }

    public String getExceptionMessage() {return exceptionMessage;}
}
