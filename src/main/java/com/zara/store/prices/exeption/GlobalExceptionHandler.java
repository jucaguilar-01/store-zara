package com.zara.store.prices.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice(basePackages = "com.zara.store.controller")
public class GlobalExceptionHandler {

    @ExceptionHandler(value={StoreBusinessException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiError handleConflict(StoreBusinessException ex) {

        ApiError message = new ApiError(
                HttpStatus.INTERNAL_SERVER_ERROR,
                ex.getExceptionMessage(),
                ex.getMessage());
        return message;
    }

    @ExceptionHandler(value={Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiError handleConflict(Exception ex) {

        ApiError message = new ApiError(
                HttpStatus.INTERNAL_SERVER_ERROR,
                "Api store unknown error",
                ex.getMessage());
        return message;
    }
}
