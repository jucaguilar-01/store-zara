package com.zara.store.prices.repository;

import com.zara.store.prices.model.PricesEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface PricesRepository extends CrudRepository<PricesEntity,Long> {

    @Query("select p from PricesEntity p where p.brandId = ?1 and p.productId = ?3 and ?2 >= p.startDate and ?2 <= p.endDate")
    List<PricesEntity> findPrice(int brandId, Timestamp startDate, long productId);
}
