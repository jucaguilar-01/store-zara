package com.zara.store.prices.service;

import com.zara.store.prices.dto.PriceDto;
import com.zara.store.prices.exeption.StoreBusinessException;

public interface PriceService {

    PriceDto getPriceService(int brandId, String startDate, long productId) throws StoreBusinessException;

}
