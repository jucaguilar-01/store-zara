package com.zara.store.prices.service.impl;

import com.zara.store.prices.dto.PriceDto;
import com.zara.store.prices.exeption.NotFoundEntity;
import com.zara.store.prices.exeption.StoreBusinessException;
import com.zara.store.prices.mapper.PriceMapper;
import com.zara.store.prices.model.PricesEntity;
import com.zara.store.prices.repository.PricesRepository;
import com.zara.store.prices.service.PriceService;
import com.zara.store.prices.utils.parserUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PriceServiceImpl implements PriceService {

    private final PricesRepository pricesRepository;

    private final PriceMapper priceMapper;

    @Override
    public PriceDto getPriceService(int brandId, String startDate, long productId) throws StoreBusinessException {
        try {
            List<PriceDto> response = priceMapper.entityToDtoList(this.pricesRepository.findPrice(brandId, parserUtils.converterTimestamp(startDate), productId));
            if (response == null || response.size() == 0) {
                throw new NotFoundEntity(NotFoundEntity.NOT_FOUND_ENTITY);
            } else if (response.size() == 1) {
                return response.get(0);
            } else {
                List<PriceDto> filter = response.stream().filter(x -> x.getPriority() == 1).collect(Collectors.toList());
                return getPriceFilter(filter);
            }
        } catch (Exception e) {
            throw new StoreBusinessException(StoreBusinessException.SERVICE_GET_PRICES, e.getMessage());
        }
    }

    private PriceDto getPriceFilter(List<PriceDto> filter) throws NotFoundEntity {
        if(filter.size() == 1){
            return filter.get(0);
        } else {
            throw new NotFoundEntity(NotFoundEntity.MULTIPLES_ENTITIES);
        }
    }
}
