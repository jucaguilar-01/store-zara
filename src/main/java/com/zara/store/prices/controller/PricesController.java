package com.zara.store.prices.controller;

import com.zara.store.prices.dto.PriceDto;
import com.zara.store.prices.exeption.StoreBusinessException;
import com.zara.store.prices.mapper.PriceMapper;
import com.zara.store.prices.response.FindPriceResponse;
import com.zara.store.prices.service.PriceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@RestController
@Validated
@RequiredArgsConstructor
@Slf4j
@RequestMapping("api/v1/store")
public class PricesController {

    private final PriceService priceService;

    private final PriceMapper priceMapper;

    @GetMapping(path = "/prices", produces =  MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FindPriceResponse> getPrice(@RequestParam("brandId") @NotNull int brandId,
                                                      @RequestParam("startDate") @NotEmpty String startDate,
                                                      @RequestParam("productId") @NotNull long productId)
            throws StoreBusinessException {
    PriceDto response = this.priceService.getPriceService(brandId,startDate,productId);
    return new ResponseEntity<>(this.priceMapper.buildFindPriceResponse(response), HttpStatus.OK);
    }
}
